#![feature(asm)]
#![no_std]
#![no_main]

// pick a panicking behavior
extern crate panic_halt; // you can put a breakpoint on `rust_begin_unwind` to catch panics
                         // extern crate panic_abort; // requires nightly
                         // extern crate panic_itm; // logs messages over ITM; requires ITM support
                         // extern crate panic_semihosting; // logs messages to the host stderr; requires a debugger

use cortex_m_rt::{entry, exception, pre_init};

use teensy_lc::hal::cop::*;
use teensy_lc::hal::mkl26z4::{self, FGPIOC, PORTC, SIM};
use teensy_lc::hal::prelude::*;

#[pre_init]
unsafe fn init() {
    let scgc5 = (*SIM::ptr()).scgc5.read().bits();
    (*SIM::ptr())
        .scgc5
        .write(|w| unsafe { w.bits(scgc5 | 1 << 11) });
    (*PORTC::ptr()).pcr5.write(|w| unsafe { w.bits(0x100) });
    (*FGPIOC::ptr()).pddr.write(|w| unsafe { w.bits(1 << 5) });
}

#[entry]
fn main() -> ! {
    let peripherals = mkl26z4::Peripherals::take().unwrap();
    let sim = peripherals.SIM.constrain();
    let mut wdog = sim
        .cop
        .configure(Timeout::Long, Source::Internal, Mode::Normal);
    let mut ctr: usize = 0;
    let mut prev: usize = 1;
    let mut prevprev: usize = 1;
    let mut on = false;
    loop {
        if ctr % 100_000 == 0 {
            wdog.kick()
        }
        if ctr == 100_000 * prevprev {
            ctr = 0;
            if on {
                let tmp = prev;
                prev = prev + prevprev;
                prevprev = tmp;
            }
            peripherals.FGPIOC.ptor.write(|w| unsafe { w.bits(1 << 5) });
            on = !on;
        }
        ctr += 1;
    }
}

#[exception]
unsafe fn DefaultHandler(e: i16) -> ! {
    let mut ctr: usize = 0;
    loop {
        ctr += 1;
        if ctr == 50_000 {
            (*FGPIOC::ptr()).ptor.write(|w| unsafe { w.bits(1 << 5) });
            ctr = 0;
        }
    }
}
