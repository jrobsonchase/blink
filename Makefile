.PHONY: all flash
all: target/thumbv6m-none-eabi/release/blink.hex

.PHONY: target/thumbv6m-none-eabi/release/blink
target/thumbv6m-none-eabi/release/blink:
	cargo build --release

target/thumbv6m-none-eabi/release/blink.hex: target/thumbv6m-none-eabi/release/blink 
	objcopy -O ihex target/thumbv6m-none-eabi/release/blink target/thumbv6m-none-eabi/release/blink.hex

flash: target/thumbv6m-none-eabi/release/blink.hex
	teensy-loader-cli --mcu TEENSYLC -w target/thumbv6m-none-eabi/release/blink.hex

dump:
	cargo objdump --release --bin blink -- -disassemble -no-show-raw-insn -print-imm-hex
